# molecule-vagrant-libvirt

This image contains all the tools you'll need to use molecule with, for example, a distant kvm host accessible with libvirt over ssh

## Getting Started

These instructions will cover usage information and for the docker container

### Prerequisites

  * A distant hypervisor addressable with libvirt. This image has been tested with kvm/qemu, but __should__ work with esxi, xen, bhyve or other hypervisor, as long as they are properly configured in molecule
  * Some molecule scenarios using libvirt in your project

__example project using molecule/vagrant/libvirt:__

https://gitlab.com/ulrich.giraud/kubernetes-bare-metal

### Usage

#### Container Parameters

Run all tests:
```shell
docker run -v $(pwd):/sources/your_role -w /sources/your_role \
  -v ~/.vagrant.d/boxes/:/root/.vagrant.d/boxes/ \
  -v /var/run/libvirt/libvirt-sock:/var/run/libvirt/libvirt-sock \
   ulrichg/molecule-vagrant-libvirt:latest
```

Run only indempotence test:

```shell
docker run  -v $(pwd):/sources/your_role -w /sources/your_role \
  -v ~/.vagrant.d/boxes/:/root/.vagrant.d/boxes/ \
  -v /var/run/libvirt/libvirt-sock:/var/run/libvirt/libvirt-sock \
  ulrichg/molecule-vagrant-libvirt:latest idempotence
```

Interactive:

```shell
docker run -v $(pwd):/sources/your_role -it --entrypoint bash ulrichg/molecule-vagrant-libvirt:latest bash
molecule converge
```

__NB__: the volume on libvirt.sock is optional, it is only needed to call libvirtd on your local system

#### Environment Variables

See molecule environment variables


## Built With

  * ubuntu:latest
  * molecule 3.1.5
  * molecule-vagrant 0.5
  * python-vagrant 0.5.15
  * testinfra 5.3.1
  * ansible 2.10.0

## Find Us

* [Gitlab](https://gitlab.com/ulrich.giraud/molecule-vagrant-libvirt)
* [docker hub](https://hub.docker.com/u/ulrichg)

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting merge requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the
[tags on this repository](https://gitlab.com/ulrich.giraud/molecule-vagrant-libvirt/tags).

## Authors

* **Ulrich GIRAUD** - *Initial work* - [ulrich.giraud](https://gitlab.com/ulrich.giraud)

See also the list of [contributors](https://gitlab.com/ulrich.giraud/molecule-vagrant-libvirt/-/graphs/master) who
participated in this project.

## License

This project is licensed under the BSD License
