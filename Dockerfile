FROM ubuntu:latest

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y ruby-dev vagrant openssh-client python3-pip libvirt-dev libvirt-clients build-essential pkg-config python3-dev \
    && apt-get clean

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN rm /usr/bin/ld && ln -fs /usr/bin/ld.gold /usr/bin/ld
RUN CONFIGURE_ARGS="with-libvirt-include=/usr/include/libvirt with-libvirt-lib=/usr/lib" vagrant plugin install vagrant-libvirt

## This ugly hack is due to a bug in vagrant-libvirt : https://github.com/vagrant-libvirt/vagrant-libvirt/issues/921
# Issue has been corrected then reverted, so we need to edit in place...
RUN sed -i 's/nc /nc -q0 /g' /root/.vagrant.d/gems/2.7.0/gems/vagrant-libvirt-0.3.0/lib/vagrant-libvirt/provider.rb

ENV PYTHONDONTWRITEBYTECODE=1
WORKDIR /sources
ENTRYPOINT ["/usr/local/bin/molecule"]
CMD ["test"]
